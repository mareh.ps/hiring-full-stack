FROM python:3

WORKDIR /app

ADD . /app

EXPOSE 8000

CMD ["python3", "-m", "http.server"]

