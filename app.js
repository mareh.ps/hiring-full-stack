let app = angular.module('app', []);

app.controller('userController' ,function($scope, $http){
    $scope.submit = function(){
        $scope.getUser($scope.user);
    }
    $scope.userData;
    $scope.userRepos;
    $scope.error;
    $scope.errorRepos;
    $scope.getUser = function(username){
        return $http.get('https://api.github.com/users/' + username).then(function(result){
            $scope.userData = result.data;  
            $scope.getRepos(username);   
            $scope.error = null;
            return result.data;
        }, function(error){
            $scope.userData = null;
            $scope.userRepos = null;
            $scope.error = error.status;
        });
    };

    $scope.getRepos = function(username){
        return $http.get('https://api.github.com/users/' + username + '/repos').then(function(result){
            $scope.userRepos = result.data;     
            $scope.errorRepos = null;
            return result.data;
        }, function(error){
            $scope.userRepos = null;     
            $scope.errorRepos = error.status;
        });
    };
});
