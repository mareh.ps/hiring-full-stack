# Test for hiring-front-end (Creditoo) - Marina Barnabá Silva

The app is meant to be run at any HTTP server. The source code includes a Dockerfile for running in a docker container with no dependencies than Docker itself.

## Instructions

## Build
To build the app run the following code: 
```sh
docker build -t teste-creditoo .
```

## Run
To run the app: 
```sh
docker run -p 8000:8000 teste-creditoo
```

